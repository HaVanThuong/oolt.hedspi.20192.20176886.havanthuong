package week1;

import java.util.Scanner;

public class PtBacNhatHaiAn {
	public static void main(String[] args) {
		int a,b,c;
		Scanner sc = new Scanner(System.in);
		System.out.println("Solve : ax^2 + bx + c = 0");
		System.out.print("Enter variable of a = ");
		a = sc.nextInt();
		System.out.print("Enter variable of b = ");
		b = sc.nextInt();
		System.out.print("Enter variable of c = ");
		c = sc.nextInt();
		int denta = b*b - 4*a*c;
		double x1 = (-(double)b + Math.sqrt(denta))/2*(double)a;
		double x2 = (-(double)b - Math.sqrt(denta))/2*(double)a;
		if(denta < 0)
		{
			System.out.println("Phuong trinh vo nghiem!");
		}
		else if(denta == 0)
		{
			System.out.println("Phuong trinh co nghiem duy nhat x = " + x2);
		}
		else {
			System.out.println("Phuong trinh co 2 nghiem phan biet x1 = " + x1 + " va x2 = " + x2);
		}
		return ;
	}
}
// pt bac 2
