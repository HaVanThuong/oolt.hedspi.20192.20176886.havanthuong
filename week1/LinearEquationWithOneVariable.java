package week1;

import java.util.Scanner;

public class LinearEquationWithOneVariable {
	public static void main(String[] args) {
		int a,b;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the first number: ");
		a = sc.nextInt();
		//sc.nextLine();
		System.out.print("Enter the second number: ");
		b = sc.nextInt();
		if(a == 0) System.out.println("No solution!");
		else {
			double x = (double)-b/(double)a;
			System.out.println("Solution is " + x);
		}
	}
}
