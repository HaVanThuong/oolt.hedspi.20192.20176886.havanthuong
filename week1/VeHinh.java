package week1;

import java.util.Scanner;

public class VeHinh {
	public static void main(String[] args) {		
		System.out.println("Canh = ");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		for(int i = n; i >= 1; i--)
		{
			if(i != 1 && i != n) {
				for(int j = 1; j <= n; j++) 
				{
					if(j == 1 || j == n || j== i) {
						System.out.print("*");
					}
					else System.out.print(" ");
				}
				System.out.println("");
			}
			else {
				for(int j = 1; j <= n; j++) {
					System.out.print("*");
				}
				System.out.println("");
			}
		}
	}
}
/*
 	*****
 	**  *
 	* * *
 	*  **
 	*****
 	Quan sat hinh thi thay hang 1 va hang 5 deu co so "*" == do dai canh.
 	Cac hang con lai thi sao chi xuat hien o cot dau tien, cot cuoi cung, va o cot bang hang.
 	Chia cac truong hop theo phan tich tren.
 */







