package week1;

import javax.swing.JOptionPane;

public class ShowTwoNumbers {
	public static void main(String[] args) {
		String strNum1,strNum2;
		double num1,num2;
		
		String strNotification = "Sum of two number ";
		
		strNum1 = JOptionPane.showInputDialog(null,
				"Please input the first number: ","Input the first number",
				JOptionPane.INFORMATION_MESSAGE);
		num1 = Double.parseDouble(strNum1);
		strNotification += strNum1 + " and ";
		
		strNum2 = JOptionPane.showInputDialog(null,
				"Please input the second number: ","Input the second number",
				JOptionPane.INFORMATION_MESSAGE);
		num2 = Double.parseDouble(strNum2);
		double sum = num1 + num2;
		strNotification += strNum2 + " = " + sum;
		
		JOptionPane.showMessageDialog(null,strNotification,"Show sum of two numbers", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
