package hust.soict.hedspi.aims.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Arrays;
import java.util.Scanner;

public class DateUtils {
	 public static boolean validateJavaDate(String strDate)
	   {
		if (strDate.trim().equals(""))
		{
		    return true;
		}
		else
		{
		    SimpleDateFormat sdfrmt = new SimpleDateFormat("MM/dd/yyyy");
		    sdfrmt.setLenient(false);
		  
		    try
		    {
		        Date javaDate = sdfrmt.parse(strDate); 
//		        System.out.println(strDate+" is valid date format");
		     
		    }
		   
		    catch (ParseException e)
		    {
//		        System.out.println(strDate+" is Invalid Date format");
		        return false;
		    }
		    return true;
		}
	   }
	public static void CompareDate ()throws ParseException{
		 SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	       
	        Scanner sc = new Scanner(System.in);
			System.out.println("Nhap 2 ngay so sanh: ");
			System.out.print("ngay 1:");
			String day1 = "";
			do {
				day1 = sc.nextLine();
				if(!validateJavaDate(day1)) {
					System.out.println("Nhap sai. Nhap lai");
					System.out.print("ngay 1: ");
				}
			}while(!validateJavaDate(day1));
			System.out.print("ngay 2: ");
			String day2 = "";
			do {
				day2 = sc.nextLine();
				if(!validateJavaDate(day2)) {
					System.out.println("Nhap sai. Nhap lai");
					System.out.print("ngay 2: ");
				}
			}while(!validateJavaDate(day2));
			Date date1 = sdf.parse(day1);
		    Date date2 = sdf.parse(day2);
	        System.out.println("date1 : " + sdf.format(date1));
	        System.out.println("date2 : " + sdf.format(date2));

	        if (date1.compareTo(date2) > 0) {
	            System.out.println(day1+" "+"sau"+" "+day2);
	        } else if (date1.compareTo(date2) < 0) {
	            System.out.println(day1+" "+"truoc"+" "+day2);
	        } else if (date1.compareTo(date2) == 0) {
	            System.out.println(day1+" "+"trung"+" "+day2);
	        } else {
	            System.out.println("???????");
	        }

	    }
  public static void DateSort() throws ParseException
  {
      Scanner sc =new Scanner (System.in);
      Date date[];
      String str[];
      System.out.println("nhap so luong ngay:");               
      int i,n = sc.nextInt();                                    
      str= new String[n];                                    
      date= new Date[n];                                      
      sc.nextLine();                              
       
      for(i=0;i<n;i++)
      {          
    	  do {
    		  str[i]=sc.nextLine();
				if(!validateJavaDate(str[i])) {
					System.out.println("Nhap sai. Nhap lai");
				}
			}while(!validateJavaDate(str[i]));
                                         
      }
      SimpleDateFormat sobj = new SimpleDateFormat("dd/MM/yyyy");
       
      for(i=0;i<n;i++)
      {
          date[i]=sobj.parse(str[i]);                         
      }

      Arrays.sort(date);                                                                            
       System.out.println("ngay sau khi sap xep:");
      for(Date date1 : date)                                 
      {                                                       
          System.out.println(sobj.format(date1));             
      }
       
  }

}

