package hust.soict.hedspi.aims.utils;
import java.time.LocalDate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
public class MyDate {

	private int day;
	private int month;
	private int year;

	private LocalDate now = LocalDate.now();

	public MyDate() {
		super();
		this.setYear(now.getYear());
		this.setMonth(Integer.toString(now.getMonthValue()));
		this.setDay(now.getDayOfMonth());
	}
	public void printCurrentDayFormatAsTable1(){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
		SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy"); 
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap ngay: ");
		String str1 = "";
			str1 = sc.nextLine();
		try {
			Date date = format.parse(str1);
			String strDate = formatDate.format(date);  
			System.out.println(strDate);
		} catch (ParseException e) {e.printStackTrace();}
	}
	public MyDate(int day, int month, int year) {
		super();
		if (this.checkYear(year) && this.checkMonth(Integer.toString(month)) != 0 && this.checkDay(day, month, year)) {
			this.setYear(year);
			this.setMonth(String.valueOf(month));
			this.setDay(day);
		} else {
			System.out.println("Input is invalid!");
		}
	}
    public void DefaultDate() {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMMM");  
		DateTimeFormatter dtfDay = DateTimeFormatter.ofPattern("dd"); 
		DateTimeFormatter dtfYear = DateTimeFormatter.ofPattern("yyyy"); 
		   LocalDateTime now = LocalDateTime.now();  
		   String month=dtf.format(now);String day= dtfDay.format(now);String year= dtfYear.format(now);
		int daytest = Integer.parseInt(day);
		if(daytest<10)
		{
		     day=day.substring(1,2);
		};
			if(daytest!=0) {
				if(daytest== 1|| daytest == 21 || daytest== 31) {
						day=day+"st";
					}
					else if(daytest == 2 ||daytest == 22) {
						day=day+"nd";
					}
				
					else if(daytest== 3 || daytest == 23) {
						day=day+"rd";
					}
					else {
						day=day+"th";
					}
		}
			System.out.println("hom nay la ngay:"+month+" "+day+" "+year);
	}
	public MyDate(String dateString) {
		super();
		String[] subStrings = dateString.split(" ");
		int year = Integer.parseInt(subStrings[2]);
		String month = subStrings[0];
		int day = Integer.parseInt(subStrings[1].replaceAll("\\D+", ""));
		if (this.checkYear(year) && this.checkMonth(month) != 0 && this.checkDay(day, this.checkMonth(month), year)) {
			this.setYear(year);
			this.setMonth(month);
			this.setDay(day);
		} else {
			System.out.println("Input is invalid!");
		}

	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if (this.checkDay(day, this.month, this.year)) {
			this.day = day;
		}
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(String month) {
		if (this.checkMonth(month) != 0) {
			this.month = this.checkMonth(month);
		}
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		if (this.checkYear(year)) {
			this.year = year;
		}
	}

	private boolean checkYear(int year) {
		if (year > 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkDay(int day, int month, int year) {
		int dateOfMonth = this.getDateOfMonth(year, month);
		if (day > 0 && day <= dateOfMonth) {
			return true;
		} else {
			return false;
		}
	}

	private int checkMonth(String month) {
		switch (month) {
		case "January":
		case "Jan":
		case "Jan.":
		case "1":
			return 1;
		case "February":
		case "Feb":
		case "Feb.":
		case "2":
			return 2;
		case "March":
		case "Mar":
		case "Mar.":
		case "3":
			return 3;
		case "April":
		case "Apr":
		case "Apr.":
		case "4":
			return 4;
		case "May":
		case "5":
			return 5;
		case "June":
		case "Jun":
		case "6":
			return 6;
		case "July":
		case "Jul":
		case "7":
			return 7;
		case "August":
		case "Aug":
		case "Aug.":
		case "8":
			return 8;
		case "September":
		case "Sep":
		case "Sept.":
		case "9":
			return 9;
		case "October":
		case "Oct":
		case "Oct.":
		case "10":
			return 10;
		case "November":
		case "Nov":
		case "Nov.":
		case "11":
			return 11;
		case "December":
		case "Dec":
		case "Dec.":
		case "12":
			return 12;
		default:
			return 0;
		}
	}

	private int getDateOfMonth(int year, int month) {
		int date = 0;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			date = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			date = 30;
			break;
		case 2:
			date = ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)) ? 29 : 28;
			break;
		default:
			date = 0;
			break;
		}
		return date;
	}

}
