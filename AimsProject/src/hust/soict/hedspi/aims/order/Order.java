package hust.soict.hedspi.aims.order;
import hust.soict.hedspi.aims.disc.DigitalVideoDisc;
import hust.soict.hedspi.aims.utils.MyDate;
import java.util.Random;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	private int qtyOrder = 0;
	public static final int MAX_LIMITTED_ORDERER = 2;
	private static int nbOrders = 0;
	private MyDate dateOrdered; 
	public Order(){
		super();
		dateOrdered =  new MyDate();
	     if(nbOrders<MAX_LIMITTED_ORDERER) {
		     nbOrders++;
	     }
	     else {
	    	 System.out.println("The order is limited");
	     }
	}	 	
	public void printOrder() {
		System.out.println("***********************Order***********************");
		System.out.print("Date:");
		dateOrdered.DefaultDate();
		System.out.println("Ordered Items:");
		for(int i = 0; i<this.getQtyOrder(); i++) {
			System.out.println(i+1+"-DVD-"+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getCategory()+
					"-"+itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+itemsOrdered[i].getCost()+"$");
		}
}
	public void printLuckyOrder() {
		for(int i = 0; i<this.getQtyOrder(); i++) {
			System.out.println(this.getQtyOrder()+2+"-DVD-"+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getCategory()+
					"-"+itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+"Free");
		}
	}
	public void printCost() {
		System.out.println("Total cost:"+this.totalCost());
		System.out.println("***************************************************");
	}
	public int getQtyOrder() {
		return qtyOrder;
	}
	public void setQtyOrder(int qtyOrder) {
		this.qtyOrder = qtyOrder;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
	    
		if(qtyOrder<MAX_NUMBERS_ORDERED) {
			itemsOrdered[this.getQtyOrder()] = disc;
			System.out.println("Item is added");
			this.setQtyOrder(this.getQtyOrder()+1);
		}
		else {
			System.out.println("The order is already full");
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.getQtyOrder()!= 0) {
			for(int i = 0; i < this.qtyOrder; i++){
	            if(this.itemsOrdered[i] == disc){
	                for(int j = i; j < this.qtyOrder - 1; j++){
	                	this.itemsOrdered[j] = this.itemsOrdered[j+1];
	                }
	                this.qtyOrder -= 1;
	                break;
	            }
			}
			System.out.println("Item has been removed from order.");
		}
		else {
			System.out.println("Order has nothing to remove. Cant remove");
		}

	}
	public float totalCost() {
		float sum = 0;
		if(this.getQtyOrder()!=0) {
			for(int i = 0; i<this.getQtyOrder(); i++) {
				sum+=itemsOrdered[i].getCost();
			}
		}
		double round = Math.round(sum*1e4)/1e4;
		sum = (float) round;
		return sum;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc [] dvdList) {
		for (int i = 0; i < dvdList.length; i++) {
			this.addDigitalVideoDisc(dvdList[i]);
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc dvd1,DigitalVideoDisc dvd2) {
		this.addDigitalVideoDisc(dvd1);
		this.addDigitalVideoDisc(dvd2);
	}
	
	public DigitalVideoDisc getALuckyItem(Order anOrder) {
		DigitalVideoDisc result = new DigitalVideoDisc("noname");
		int range = this.qtyOrder;
		int rand =(int)(Math.random()*range);
		for(int i = 0; i <qtyOrder; i++) {
			if(i==rand) {
				result = itemsOrdered[i];
				break;
			}
		}
		return result;
	}
}

