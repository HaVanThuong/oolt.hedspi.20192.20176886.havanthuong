package Week4;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

public class DateUtils {
	
	
	public static void dateCompare(MyDate Date1, MyDate Date2) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date1 = sdf.parse(Date1.getDay()+"/"+Date1.getMonth()+"/"+Date1.getYear());
		Date date2 = sdf.parse(Date2.getDay()+"/"+Date2.getMonth()+"/"+Date2.getYear());
		
		 if(date1.compareTo(date2)>0){
             System.out.println("Date1 is after Date2");
         }else if(date1.compareTo(date2)<0){
             System.out.println("Date1 is before Date2");
         }else{
             System.out.println("Date1 is equal to Date2");
         }
		
	}
	
	public static void sortDate(MyDate []dates ) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date[] tmp = new Date[dates.length];
        Calendar[] calendar = new Calendar[dates.length];
        
		for( int i = 0; i< dates.length; i++) {
			tmp[i] = sdf.parse(dates[i].getDay()+"/"+dates[i].getMonth()+"/"+dates[i].getYear());
			calendar[i] = Calendar.getInstance();
			calendar[i].setTime(tmp[i]);

		}
		Arrays.sort(calendar);
		for(int i = 0; i< tmp.length; i++) {
			
				System.out.print(calendar[i].get(Calendar.DAY_OF_MONTH)+"/"+(calendar[i].get(Calendar.MONTH)+1)+"/"+calendar[i].get(Calendar.YEAR) + " ");
			
			
		}
	}
}