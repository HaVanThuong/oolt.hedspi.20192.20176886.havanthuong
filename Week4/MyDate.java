package Week4;

import java.util.*;
import java.lang.*;
public class MyDate {
	private int day;
	private int month;
	private int year;
	private static final String[][] MONTHS = {
            {"January",   "Jan" , "01"},
            {"February",   "Feb" , "02"},
            {"March",      "Mar" , "03"},
            {"April",      "Apr" , "04"},
            {"May",         "May" , "05"},
            {"June",       "Jun" , "06"},
            {"July",       "Jul" , "07"},
            {"August",     "Aug" , "08"},
            {"September",  "Sep" , "09"},
            {"October",    "Oct" , "10"},
            {"November",   "Nov" , "11"},
            {"December",   "Dec" , "12"}
    };
    
    
    
    private static final String[][] DAYS = {
    		{"first",			"01"},
    		{"second",			"02"},
    		{"third",			"03"},
    		{"forth",			"04"},
    		{"fifth",			"05"},
    		{"sixth",			"06"},
    		{"seventh",			"07"},
    		{"eighth",			"08"},
    		{"ninth",			"09"},
    		{"tenth",			"10"},
    		{"eleventh",		"11"},
    		{"twelfth",			"12"},
    		{"fourteenth",		"13"},
    		{"fifteenth",		"14"},
    		{"thirdteenth",		"15"},
    		{"sixteenth",		"16"},
    		{"seventeenth",		"17"},
    		{"eighteenth",		"18"},
    		{"nineteenth",		"19"},
    		{"twentieth",		"20"},
    		{"twenty-first",	"21"},
    		{"twenty-second",	"22"},
    		{"twenty-third",	"23"},
    		{"twenty-forth",	"24"},
    		{"twenty-fifth",	"25"},
    		{"twenty-sixth",	"26"},
    		{"twenty-seventh",	"27"},
    		{"twenty-eighth",	"28"},
    		{"twenty-ninth",	"29"},
    		{"thirtieth",		"30"},
    		{"thirty-first",	"31"}	
    };
    private static final String[][] YEARSFIRST2 = {
    		{"onehundred"   ,			"01"},
    		{"twohundred"   ,			"02"},	
    		{"threehundred" ,			"03"},
    		{"fourhundred"  ,			"04"},
    		{"fivehundred"  ,			"05"},
    		{"sixhundred"   ,			"06"},
    		{"sevenhundred" ,			"07"},
    		{"eighthundred" ,			"08"},
    		{"ninehundred"  ,			"09"},
    		{"ten"          ,			"10"},
    		{"eleven"		,           "11"},
    		{"twelve"		,           "12"},
    		{"thirdteen"    ,           "13"},
    		{"fourteen"		,           "14"},
    		{"fifteen"		,           "15"},
    		{"sixteen"		,           "16"},
    		{"seventeen"	,			"17"},
    		{"eighteen"		,			"18"},
    		{"nineteen"		,			"19"},
    		{"twenty"		,			"20"},
    		{"twenty-one"	,			"21"},
    		{"twenty-two"	,			"22"},
    		{"twenty-three"	,			"23"},
    		{"twenty-four"	,			"24"},
    		{"twenty-five"	,			"25"},
    		{"twenty-six"	,			"26"},
    		{"twenty-seven"	,			"27"},
    		{"twenty-eight"	,			"28"},
    		{"twenty-nine"	,			"29"},
    		{"thirty"		,			"30"},
    };
    private static final String[][] YEARSLAST2 = {
    		{"one"   		,			"01"},
    		{"two"   		,			"02"},	
    		{"three" 		,			"03"},
    		{"four"  		,			"04"},
    		{"five"  		,			"05"},
    		{"six"   		,			"06"},
    		{"seven" 		,			"07"},
    		{"eight" 		,			"08"},
    		{"nine"  		,			"09"},
    		{"ten"          ,			"10"},
    		{"eleven"		,           "11"},
    		{"twelve"		,           "12"},
    		{"thirdteen"    ,           "13"},
    		{"fourteen"		,           "14"},
    		{"fifteen"		,           "15"},
    		{"sixteen"		,           "16"},
    		{"seventeen"	,			"17"},
    		{"eighteen"		,			"18"},
    		{"nineteen"		,			"19"},
    		{"twenty"		,			"20"},
    		{"twenty-one"	,			"21"},
    		{"twenty-two"	,			"22"},
    		{"twenty-three"	,			"23"},
    		{"twenty-four"	,			"24"},
    		{"twenty-five"	,			"25"},
    		{"twenty-six"	,			"26"},
    		{"twenty-seven"	,			"27"},
    		{"twenty-eight"	,			"28"},
    		{"twenty-nine"	,			"29"},
    		{"thirty"		,			"30"},
    };
	public MyDate() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		this.day = calendar.get(Calendar.DATE);
		this.month = calendar.get(Calendar.MONTH) ;
		this.year = calendar.get(Calendar.YEAR);
	}
	public MyDate(int d, int m, int y) {
		this.day=d;
		this.month=m;
		this.year=y;
	}
	public void accept() {
		String s;
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter date ");
		s = scan.nextLine();
		String strD;
		String strM;
		String strY = s.substring(s.length()-4, s.length());
		if (s.indexOf(" ")==s.length()-9) {
			strD = s.substring(s.length()-8, s.length()-7);
			strM = s.substring(0, s.length()-9);
		}
		else {
			strD = s.substring(s.length()-9, s.length()-8);
			strM = s.substring(0, s.length()-10);
		}
		
		int y = Integer.parseInt(strY);
		int d = Integer.parseInt(strD);
		int m=0;
		
		if (strM.equalsIgnoreCase("January")){m=1;} 
		if (strM.equalsIgnoreCase("February")){m=2;}
		if (strM.equalsIgnoreCase("March")){m=3;}
		if (strM.equalsIgnoreCase("April")){m=4;}
		if (strM.equalsIgnoreCase("May")){m=5;} 
		if (strM.equalsIgnoreCase("Jun")){m=6;}
		if (strM.equalsIgnoreCase("July")){m=7;}
		if (strM.equalsIgnoreCase("August")){m=8;} 
		if (strM.equalsIgnoreCase("September")){m=9;}
		if (strM.equalsIgnoreCase("October")){m=10;} 
		if (strM.equalsIgnoreCase("November")){m=11;} 
		if (strM.equalsIgnoreCase("December")){m=12;} 
			
		
		
		setYear(y);
		setMonth(m);
		setDay(d);
		
		scan.close();
	}
	
	public int getDay() {
		return day;
	}
	public void setDay(int d) {
		switch(month) {
			case 1: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 2: {
				if (this.year %4 != 1) {
					if (d > 0 && d <30) {
						this.day = d;
					}
				}  
				else if (d > 0 && d < 29) {
					this.day = d;
				} 
				else
					this.day = 0;
				break;
			}
			case 3: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 4: {
				if (d > 0 && d < 31) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 5: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 6: {
				if (d > 0 && d < 31) {
					this.day= d;
				} else
					this.day = 0;
				break;
			}
			case 7: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 8: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 9: {
				if (d > 0 && d < 31) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 10: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 11: {
				if (d > 0 && d < 31) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
			case 12: {
				if (d > 0 && d < 32) {
					this.day = d;
				} else
					this.day = 0;
				break;
			}
		}
	}
	
	public int getMonth() {
		return month;
	}
	public void setMonth(int m) {
		if (m > 0 && m < 13) {
			this.month = m;
		} else
			this.month = 0;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int y) {
		if (y > 0) {
			this.year = y;
		} else 
			this.year = 0;
	}
	public boolean isValidDate() {
		if ((year != 0) || (month != 0) || (day != 0))
			return true;
		return false;
	}
	
	public void print() {
		
		System.out.println("Current date: " + this.year + "-" + this.month + "-" + this.day);
	}
	public MyDate(String day, String month , String year, int format) {
		int dayNumb=turnDayToNumb(day);
		int monthNumb=turnMonthToNumb(month);
		 int yearNumb=turnYearToNumb(year);
		this.day=dayNumb;
		this.month=monthNumb;
		this.year=yearNumb;
		print(dayNumb,monthNumb,yearNumb,format);
		
	}
	public int turnDayToNumb(String day) {
		for (int i=0;i<31;i++) {
			if (DAYS[i][0].equalsIgnoreCase(day)) {
				return i+1;
			}
		}
		return 0;
	}
	public int turnMonthToNumb(String month) {
		for (int i=0;i<12;i++) {
			if (MONTHS[i][0].equalsIgnoreCase(month)) {
				return i+1;
			}
		}
		return 0;
	}
	public int turnYearToNumb(String year) {
		int total=0;
		String first2;
		String last2;
		int m = year.indexOf(" ");
		first2=year.substring(0,m);
		last2=year.substring(m+1,year.length());
		for (int i=0;i<30;i++) {
			if (YEARSFIRST2[i][0].equalsIgnoreCase(first2)) {
				total=total+(i+1)*100;
			}
		}
		for (int i=0;i<30;i++) {
			if (YEARSLAST2[i][0].equalsIgnoreCase(last2)) {
				total=total+i+1;
			}
		}
		return total;
	}
	public void print(int day, int month, int year , int format) {
		switch(format) {
			case 1 : {
				System.out.println("Current date: " + year + "-" + MONTHS[month-1][2] + "-" + DAYS[day-1][1]);
				break;
			}
			case 2 :{
				System.out.println("Current date: " + DAYS[day-1][1] + "/" + MONTHS[month-1][2] + "/" + year);
				break;
			}
			case 3 : {
				System.out.println("Current date: " + DAYS[day-1][1] + "-" + MONTHS[month-1][1] + "-" + year);
				break;
			}
			case 4 : {
				System.out.println("Current date: " + MONTHS[month-1][1] + " " + DAYS[day-1][1] + " " + year);
				break;
			}
			case 5 :{
				System.out.println("Current date: " + MONTHS[month-1][2] + "-" + DAYS[day-1][1] + "-" + year);
				break;
			}
		}
		
	}
	
}
