package week2;

import java.util.Scanner;

public class TwoMatricesOfSameSize {
	public static void main(String args[]){  
		//creating two matrices
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the size of two matrices: ");
		int n = sc.nextInt();
		int a[][] = new int[n][n];    
		int b[][] = new int[n][n];    
		    
		//creating another matrix to store the sum of two matrices    
		int c[][]=new int[n][n];  //n rows and n columns  
		    
		//adding and printing addition of 2 matrices    
		for(int i=0;i<n;i++){    
			for(int j=0;j<n;j++){    
				System.out.print("Enter a" + i + j + ": ");
				a[i][j] = sc.nextInt();    //use - for subtraction
			}    
		} 
		for(int i=0;i<n;i++){    
			for(int j=0;j<n;j++){    
				System.out.print("Enter b" + i + j + ": ");
				b[i][j] = sc.nextInt();    //use - for subtraction
			}    
		}
		System.out.println("Matrice A : ");
		for(int i=0;i<n;i++){    
			for(int j=0;j<n;j++){    
				System.out.print(a[i][j] + " ");
			}    
			System.out.println();
		}
		System.out.println("Matrice B : ");
		for(int i=0;i<n;i++){    
			for(int j=0;j<n;j++){    
				System.out.print(b[i][j] + " ");
			}    
			System.out.println();
		}
	}
}
