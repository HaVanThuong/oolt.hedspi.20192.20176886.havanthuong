package week2;

import java.util.Scanner;

public class DisplayTheNumberOfDaysOfAMonth {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String month;
		int year;
		boolean b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12;		
		boolean c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12;
		boolean d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12;
		
		// Enter month
		do {
			System.out.print("Enter month : ");
			month = sc.nextLine();
			b1 = month.equals("Jan");
			b2 = month.equals("Feb");
			b3 = month.equals("Mar");
			b4 = month.equals("Apr");
			b5 = month.equals("May");
			b6 = month.equals("Jun");
			b7 = month.equals("Jul");
			b8 = month.equals("Aug");
			b9 = month.equals("Sep");
			b10 = month.equals("Oct");
			b11 = month.equals("Nov");
			b12 = month.equals("Dec");
			
			c1 = month.equals("Jan.");
			c2 = month.equals("Feb.");
			c3 = month.equals("Mar.");
			c4 = month.equals("Apr.");
			c5 = month.equals("May.");
			c6 = month.equals("Jun.");
			c7 = month.equals("Jul.");
			c8 = month.equals("Aug.");
			c9 = month.equals("Sep.");
			c10 = month.equals("Oct.");
			c11 = month.equals("Nov.");
			c12 = month.equals("Dec.");
			
			d1 = month.equals("1");
			d2 = month.equals("2");
			d3 = month.equals("3");
			d4 = month.equals("4");
			d5 = month.equals("5");
			d6 = month.equals("6");
			d7 = month.equals("12");
			d8 = month.equals("7");
			d9 = month.equals("8");
			d10 = month.equals("9");
			d11 = month.equals("10");
			d12 = month.equals("11");
	
			if(b1 == false && b2 == false && b3 == false && b4 == false && b5 == false && b6 == false && b7 == false && b8 == false && b9 == false && b10 == false && b11 == false && b12 == false &&
			   c1 == false && c2 == false && c3 == false && c4 == false && c5 == false && c6 == false && c7 == false && c8 == false && c9 == false && c10 == false && c11 == false && c12 == false &&
			   d1 == false && d2 == false && d3 == false && d4 == false && d5 == false && d6 == false && d7 == false && d8 == false && d9 == false && d10 == false && d11 == false && d12 == false) { 				
				System.out.println("Month ERROR!");
			}
		}while(b1 == false && b2 == false && b3 == false && b4 == false && b5 == false && b6 == false && b7 == false && b8 == false && b9 == false && b10 == false && b11 == false && b12 == false &&
			   c1 == false && c2 == false && c3 == false && c4 == false && c5 == false && c6 == false && c7 == false && c8 == false && c9 == false && c10 == false && c11 == false && c12 == false &&
			   d1 == false && d2 == false && d3 == false && d4 == false && d5 == false && d6 == false && d7 == false && d8 == false && d9 == false && d10 == false && d11 == false && d12 == false);
		
		
		//Enter year
		do {
			System.out.print("Enter year : ");
			year = sc.nextInt();
			if(year <= 0) {
				System.out.println("Year ERROR!");
			}
		}while(year <= 0);
		
		
		// 
		if(month.equals("Jan") == true || month.equals("Mar") == true || month.equals("May") == true || month.equals("Jul") == true || month.equals("Aug") == true || month.equals("Oct") == true || month.equals("Dec") == true ||
		   month.equals("Jan.") == true || month.equals("Mar.") == true || month.equals("May.") == true || month.equals("Jul.") == true || month.equals("Aug.") == true || month.equals("Oct.") == true || month.equals("Dec.") == true ||
		   month.equals("1") == true || month.equals("3") == true || month.equals("5") == true || month.equals("7") == true || month.equals("8") == true || month.equals("10") == true || month.equals("12") == true) {
			System.out.println("Month " + month + " in " + year + " has " + 31 + " days!");
		}
		else if (month.equals("Apr") == true || month.equals("Jun") == true || month.equals("Sep") == true || month.equals("Nov") == true ||
				month.equals("Apr.") == true || month.equals("Jun.") == true || month.equals("Sep.") == true || month.equals("Nov.") == true ||
				month.equals("4") == true || month.equals("6") == true || month.equals("9") == true || month.equals("11") == true) {
			System.out.println("Month " + month + " in " + year + " has " + 30 + " days!");
		}
		// Leap year or common year
		else {
			if(year % 4 != 0) {
				System.out.println("February in " + year + " has 28 days!");
			}
			else {
				if(year % 100 == 0 && year % 400 != 0) {
					System.out.println("February in " + year + " has 28 days!");
				}
				else {
					System.out.println("February in " + year + " has 29 days!");
				}
			}
		}
			
	}
}
