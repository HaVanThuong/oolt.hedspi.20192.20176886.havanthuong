package week2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Sort {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] array = new int[5];
		int i;
		int tong = 0;	
		for(i = 0; i <= 4; i++)
		{
			System.out.println("Nhap vao phan tu thu " + i);
			array[i] = sc.nextInt();
			tong+= array[i];
		}
		System.out.println("Tong cua mang = " + tong);
		System.out.println("Mang truoc khi sap xep la : " + Arrays.toString(array));
		Arrays.parallelSort(array);
		System.out.println("Mang sau khi sap xep la : " + Arrays.toString(array));
		return;
	}
}
